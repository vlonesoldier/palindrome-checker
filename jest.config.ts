import type {Config} from 'jest';

const config: Config = {
    automock: true,
    collectCoverage: true,
    collectCoverageFrom: [
        "src/**/*.{js,jsx}",
        "src/**/*.{ts,tsx}"
    ],
    preset: "ts-jest",
    verbose: true
};

export default config;