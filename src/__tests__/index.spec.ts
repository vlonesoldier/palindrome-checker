import type * as TestClass from "../index";

const { PalindromeChecker } = jest.requireActual("../index.ts");

describe("Check if palindrome", () => {
    let potentialPalindromeObj: Array<typeof PalindromeChecker>;

    beforeAll(() => {
        potentialPalindromeObj = [
            new PalindromeChecker("rotator"),
            new PalindromeChecker("deed"),
            new PalindromeChecker("noon"),
            new PalindromeChecker("thermal isolation"),
            new PalindromeChecker("whole shabang"),
        ];
    })

    it("It is a palindrome", () => {
        
        expect(potentialPalindromeObj[0]).toBeDefined();
        expect(potentialPalindromeObj[0].isPalindrome()).toBe(true);
        
        expect(potentialPalindromeObj[1]).toBeDefined();
        expect(potentialPalindromeObj[1].isPalindrome()).toBe(true);
        
        expect(potentialPalindromeObj[2]).toBeDefined();
        expect(potentialPalindromeObj[2].isPalindrome()).toBe(true);
    })

    it("It is not a palindrome", () => {

        expect(potentialPalindromeObj[3]).toBeDefined();
        expect(potentialPalindromeObj[3].isPalindrome()).toBe(false);
        
        expect(potentialPalindromeObj[4]).toBeDefined();
        expect(potentialPalindromeObj[4].isPalindrome()).toBe(false);

    })
})