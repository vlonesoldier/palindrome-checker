export class PalindromeChecker {
    str: string;

    constructor(str: string) {
        this.str = str;
    }
    isPalindrome(): boolean {
        return this.checkIfTheSameBackwards(this.str);
    }
    checkIfTheSameBackwards(str: string): boolean {
        for (let i = 0; i < str.length; i++) {
            if (str[i] !== str[str.length - 1 - i])
                return false;
        }
        return true;
    }
}